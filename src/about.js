import React,{useState,useEffect} from 'react';
import {SearchBox} from './components/search-box/search-box.component';
import {Cardlist} from './components/card-list/card-list.component'

function Hello(props) {

    useEffect(() =>{
        console.log("props ", props.name);
    },[])

    useEffect(() =>{
        console.log("props update", props.name);
    },[props.name])

    return(
        <div>
            <h1>{props.name}</h1>
        </div>
    );
}

function Getvalue(){
    const [val,setValue] = useState("");

    return(
        <div>
            <h1>Form</h1>
            <input type="text" value={val} onChange={(e)=>{setValue(e.target.value)}}/>
            <button onClick={()=>{alert(val); setValue("")}}>Click</button>
        </div>
    );
}

class About extends React.Component {
    constructor(){
        super();
        this.state = {
            name:'Harsh',
            age: 20
        }
    }

    componentDidMount(){
        console.log("componentDidMount")
    }

    componentDidUpdate(){
        console.log("componentDidUpdate")
    }

    render(){
        return (
            <div>
                <h1>{this.state.name}</h1>
                <h2>{this.props.name}</h2>
            </div>
        );
    }
}

class Monster extends React.Component{
    constructor(){
        super();
        this.state = {
            monsters :[],
            searchField:''
        };
    }

    componentDidMount(){
        fetch("https://jsonplaceholder.typicode.com/users")
        .then(response => response.json())
        .then(user => this.setState({monsters:user}));
    }

    handleChange = (e) => {
        this.setState({searchField:e.target.value})
    }

    render(){
        const { monsters, searchField } = this.state;
        const filterMonsters = monsters.filter(monsters =>
            monsters.name.toLowerCase().includes(searchField.toLocaleLowerCase())    
        );

        return(
            <div>
                <h1>Monster Rolodex</h1>
                <SearchBox placeholder="search monster" handleChange={this.handleChange}/>
                <Cardlist monsters={filterMonsters}/>
            </div>
        )
    }
}

export default Monster;